# clear screen
alias cl='clear'
# p3 was not present by default
alias p3='python3'
# Ubuntu/Debian repository refresh
alias u='sudo apt update'
# Ubuntu/Debian install all packages updates
alias dstu='sudo apt upgrade'
# search in repository for packages
alias s='apt-cache search'
# install package on Linux
alias i='sudo apt-get install'
# Ruby on Rails
alias bi='bin/bundle install'
alias bu='bin/bundle update'
alias rls='RAILS_ENV=development rails server'
alias rlsp='RAILS_ENV=production rails s'
alias acp='RAILS_ENV=production rails assets:precompile'
# Git 
alias cln='git clone'
alias pl='git pull'
alias cmm='git commit -m'
# some issue in autocompleting branch names if using below git aliases
alias gplo='git pull origin'
alias gpso='git push origin'
# Misc
alias x='exit'
alias po='poweroff'
alias dkr='sudo docker'
alias vps='ssh root@<ip_here>'